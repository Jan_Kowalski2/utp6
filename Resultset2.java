import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class Resultset2 {

  static public void main(String[] args) {
    new Resultset2();
  }

  Statement stmt;

	Resultset2()  {
	   Connection con = null;
	   String url = "jdbc:derby:C:/DerbyDbs/Ksidb";
	   try {
		 con = DriverManager.getConnection(url);
	   } catch (Exception exc)  {
	     System.out.println(exc);
	     System.exit(1);
	   }
	   
	   String sel = "SELECT pozyc.TYTUL, pozyc.ROK, pozyc.CENA, aut.NAME FROM POZYCJE pozyc INNER JOIN AUTOR aut ON pozyc.AUTID = aut.AUTID WHERE pozyc.rok > 2000 AND pozyc.cena > 30";
	   try  {
	      Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                  ResultSet.CONCUR_READ_ONLY);
	      ResultSet rs = stmt.executeQuery(sel);
	      ResultSetMetaData rsmd = rs.getMetaData();
	      int cc = rsmd.getColumnCount();
	      rs.afterLast();	  
	      while (rs.previous()) {
	    	  for (int i = 1; i <= cc; i++) {
	  	        System.out.print(rsmd.getColumnLabel(i) + "     ");
	    	  	System.out.println(rs.getString(i));
	    	  }
	      }
	   
	      System.out.println("\n------------------------------ przewijanie do g�ry");
	      
	      System.out.println("\n----------------------------- pozycjonowanie abs.");
	      int[] poz =  { 3, 7, 9  };
	      for (int p = 0; p < poz.length; p++)  {
	         System.out.print("[ " + poz[p] + " ] ");
	         rs.absolute(poz[p]);
	         for (int i = 1; i <= cc; i++) System.out.print(rs.getString(i) + ", ");
	         System.out.println("");
	      }
	      stmt.close();
	      con.close();
	   } catch (SQLException exc)  {
	     System.out.println(exc.getMessage());
	   }

	}
} 
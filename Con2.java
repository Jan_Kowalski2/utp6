import java.sql.*;
import java.lang.reflect.*;

public class Con2  {

	private String url = "jdbc:derby://localhost:1527/ksidb";
	private Connection connection = null;
	private DatabaseMetaData md = null;

	 public Con2()  {
		 try {
			 this.connection = DriverManager.getConnection(this.url);
			 this.md = this.connection.getMetaData();
			 reportInfo();
		 } catch(SQLException e) {
			 e.printStackTrace();
		 }
	 }
	
	// Metoda raportuj�ca informacje zebrane w DatabaseMetaData
	// w wywo�aniach metody info podano jako argumenty nazwy metod tego interfejsu
	// a w metodzie info korszystamy z metod refleksji;
	// ten spos�b oprogramowania jest zaawansowany, ale wygodny, bo du�o mniej pisania
	// i kod jest bardziej klarowny
	// klauzula throws SQLException m�wi o tym, �e w trakcie dzia�ania reportInfo mo�e powsta� wyj�tek
	// SQLException, ale nie b�dziemy go tu obs�ugiwa�, obs�ug� przeka�emy do miejsca wywo�ania
	// czyli bloku try w konstruktorze

	 void reportInfo() throws SQLException {
        info("getDatabaseProductName");
        info("getDatabaseProductVersion");
        info("getDriverName");
        info("getURL");
        info("getUserName");

        info("supportsAlterTableWithAddColumn");
        info("supportsAlterTableWithDropColumn");
        info("supportsANSI92FullSQL");
        info("supportsBatchUpdates");
        info("supportsMixedCaseIdentifiers");
        info("supportsMultipleTransactions");
        info("supportsPositionedDelete");
        info("supportsPositionedUpdate");
        info("supportsSchemasInDataManipulation");
        info("supportsTransactions");

        System.out.println("ResultSet  TYPE_SCROLL_INSENSITIVE :" + md.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE));
        System.out.println("ResultSet  TYPE_SCROLL_SENSITIVE :" + md.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE));
        System.out.println("insertsAreDetected :" + md.insertsAreDetected(ResultSet.TYPE_SCROLL_INSENSITIVE));
        System.out.println("updatesAreDetected :" + md.updatesAreDetected(ResultSet.TYPE_SCROLL_INSENSITIVE));
    }

	// Metoda info korzysta z metod refleksji do wywo�ania metod podanych "przez" nazwy.
	void info(String metName) {
        Class<?> mdc  = DatabaseMetaData.class;
        Class<?>[] paramTypes =  { };
        Object[] params =  { };
        String infoTyp;
        if (metName.startsWith("get"))
            infoTyp = metName.substring(3,metName.length());
        else infoTyp = metName;
        try  {
            Method m = mdc.getDeclaredMethod(metName, paramTypes);
            System.out.println(infoTyp + ": " + m.invoke(md, params));  // dynamiczne wywo�anie metody
        } catch(Exception exc)  {   // Mo�liwe powody wyj�tk�w: nie ma takiej metody, niew�a�ciwe wywo�anie
            System.out.println(exc);
        }
    }
}
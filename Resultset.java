import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Resultset {

  static public void main(String[] args) {
    new Resultset();
  }

  Statement stmt;

	Resultset()  {
	   Connection con = null;
	   String url = "jdbc:derby:C:/DerbyDbs/Ksidb";
	   try {
		 con = DriverManager.getConnection(url);
	   } catch (Exception exc)  {
	     System.out.println(exc);
	     System.exit(1);
	   }
	   
	   String sel = "SELECT pozyc.TYTUL, pozyc.ROK, pozyc.CENA, aut.NAME FROM POZYCJE pozyc INNER JOIN AUTOR aut ON pozyc.AUTID = aut.AUTID WHERE pozyc.rok > 2000 AND pozyc.cena > 30";
	   try  {
	      Statement stmt = con.createStatement();
	      ResultSet rs = stmt.executeQuery(sel);
	      while (rs.next())  {
	         String nazwisko = rs.getString("NAME");
	         String tytul = rs.getString("TYTUL");
	         String rok = rs.getString("ROK");
	         String cena = rs.getString("CENA");
	         System.out.println("Autor: " + nazwisko);
	         System.out.println("Tytul: " + tytul);
	         System.out.println("Rok: " + rok);
	         System.out.println("Cena: " + cena);
	      }
	      stmt.close();
	      con.close();
	   } catch (SQLException exc)  {
	     System.out.println(exc.getMessage());
	   }

	}
} 
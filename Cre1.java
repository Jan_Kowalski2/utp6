import java.sql.*;

public class Cre1 {

  static public void main(String[] args) {
    new Cre1();
  }

	Statement stmt; 
	Connection con = null;
	
	Cre1()  {
		String url = "jdbc:derby:C:/DerbyDbs/Ksidb";
	
	   try {
		   con = DriverManager.getConnection(url);
	   } catch (Exception exc)  {
	     System.out.println(exc);
	     System.exit(1);
	   }
	
	   // metoda dropTable jest nasz� w�asn� metod� napisan� dla skr�cenia programu
	   // usuwa ona tabel� podan� jako argument
	   // Aby w ka�dych okoliczno�ciach stworzy� now� tabel� WYDAWCA
	   // musimy usun�� ew.  ju� istniej�c� tabel� WYDAWCA
//	   dropTable("POZYCJE"); // usuni�cie tabeli pochodnej, b�d�cej w relacji z tabel� WYDAWCA
//	   dropTable("WYDAWCA"); // usini�cie tabeli WYDAWCA
	
	   String crestmt = "create table AUTOR ("
	   		+ "AUTID integer not null AUTO_INCREMENT,"
	   		+ "NAME varchar(255) not null,"
	   		+ "PRIMARY KEY(AUTID)"
	   		+ ")";
	
	   try  {
		   stmt = con.createStatement();
		   stmt.executeUpdate(crestmt);
           // wykonanie polecenia zapisanego w crestmt
	
	   } catch (SQLException exc)  {                      // przechwycenie wyj�tku:
	      System.out.println("SQL except.: " + exc.getMessage());
	      System.out.println("SQL state  : " + exc.getSQLState());
	      System.out.println("Vendor errc: " + exc.getErrorCode());
	      System.exit(1);
	   } finally {
	      try {
	        stmt.close();
	        con.close();
	      } catch(SQLException exc) {
	        System.out.println(exc);
	        System.exit(1);
	      }
	   }
	}
	
	private void dropTable(String tname)  {
		try  {
			 stmt = con.createStatement();
			 stmt.executeUpdate("DROP TABLE " + tname);	
			
		   } catch (SQLException exc)  {                      // przechwycenie wyj�tku:
			  
		      System.out.println("SQL except.: " + exc.getMessage());
		      System.out.println("SQL state  : " + exc.getSQLState());
		      System.out.println("Vendor errc: " + exc.getErrorCode());
		      System.exit(1);
		   } finally {
		      try {
		        stmt.close();
		        con.close();
		      } catch(SQLException exc) {
		        System.out.println(exc);
		        System.exit(1);
		      }
		   }
	}
}
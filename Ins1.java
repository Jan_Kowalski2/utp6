import java.sql.*;

public class Ins1 {

  static public void main(String[] args) {
    new Ins1();
  }

  Statement stmt;

	Ins1()  {
	   Connection con = null;
	   String url = "jdbc:derby:C:/DerbyDbs/Ksidb";
	   try {
		 con = DriverManager.getConnection(url);
	   } catch (Exception exc)  {
	     System.out.println(exc);
	     System.exit(1);
	   }
		// nazwy wydawc�w do wpisywania do tabeli
	   String[] wyd =  { "PWN", "PWE", "Czytelnik", "Amber", "HELION", "MIKOM" };
	
	   	// pierwszy numer wydawcy do wpisywania do tabeli: PWN ma numer 15, PWE ma 16, ...
	   int beginKey = 15;
	
	   String[] ins = {"INSERT INTO WYDAWCA (WYDID, NAME) VALUES (", ", '", "')"}; // ? ... tablica instrukcji SQL do wpisywania rekord�w do tabeli: INSERT ...                  
	   int insCount = 0;   // ile rekord�w wpisano
	   int delCount = 0;
	   
	   try  {
		   
	     for (int i = 0; i < wyd.length; i++) {
	    	 stmt = con.createStatement();
			 stmt.executeUpdate(ins[0] + (beginKey + i) + ins[1] + wyd[i] + ins[2]);	 
			 insCount++;
	     }
	     
	     PreparedStatement ps = con.prepareStatement("DELETE FROM WYDAWCA WHERE WYDID = ?");
	     for (int i = 0; i < wyd.length; i++) {
	    	 ps.setString(1, String.valueOf(beginKey + i));
	    	 ps.executeUpdate();
	    	 delCount++;
	     }
	    
	   }
	   catch (SQLException exc)  {                      // przechwycenie wyj�tku:	  
	      System.out.println("SQL except.: " + exc.getMessage());
	      System.out.println("SQL state  : " + exc.getSQLState());
	      System.out.println("Vendor errc: " + exc.getErrorCode());
	      System.exit(1);
	   }
	//...
	}
} 